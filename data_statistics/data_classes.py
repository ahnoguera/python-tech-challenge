
def check_small_positive_decorator(func):
    def func_wrapper(cls, x, y=None):
        _y = 0 if y == None else y
        if not type(x) == int or not type(_y) == int:
            raise TypeError("Type error. The value must be an integer.")
        
        if x >= 1000 or x < 0:
            raise ValueError('Incorrect value. The number must be greater than or equal to 0 and less than 1000.')
        
        if _y >= 1000 or _y < 0:
            raise ValueError('Incorrect value. The number must be greater than or equal to 0 and less than 1000.')
        
        if y == None:
            return func(cls, x)
        return func(cls, x, y)
    return func_wrapper



class DataCapture():
    '''
    Class that acept small positive integers (I asumming 0 is an integer)
    '''
    def __init__(self) -> None:
        self.list = []
    
    class DataStat():
        '''Class to compute some basic statistics over the collection of small positive integers'''
        def __init__(self, l_val:list=[]) -> None:
            self.list = l_val
            self.max = 0
            self.min = 0
            self.histogram = []
            self.gt_list = []
            self.lt_list = []

            if self.list:
                self.max = max(self.list) # O(n)
                self.min = min(self.list) # O(n)
                self.histogram = [0] * (self.max + 1)
                
            #Histogram
            for i in self.list: # O(n)
                self.histogram[i] += 1
            
            self.gt_list = self.histogram.copy()
            self.lt_list = self.histogram.copy()
            
            
            
            for i in range(1, len(self.histogram)): # O(n)
                #Cumulative increasing, to implement less() method
                # For each i element( i >= 1) in list L, L[i] = L[i] + L[i-1].
                # Since the indices represent the values query. The idea behind this "cumulative increasing" processes
                # is to store at each index the amount of numbers (from the original list) less than or equal to the index.
                self.lt_list[i] = self.lt_list[i] + self.lt_list[i - 1] # O(1)
                
                # Cumulative decreasing, to implementgreater() method
                # Same idea, but starting from the end of the list
                j = len(self.histogram) - 1 - i
                self.gt_list[j] = self.gt_list[j] + self.gt_list[j + 1] # O(1)

        # Methods
        @check_small_positive_decorator
        def less(self, val:int) -> int:
            if val <= self.min:
                return 0
            
            if val > self.max:
                return self.lt_list[self.max]
            
            return self.lt_list[val-1]
        
        @check_small_positive_decorator
        def greater(self, val:int) -> int:
            if val >= self.max:
                return 0
            
            if val < self.min:
                return self.gt_list[self.min]
            
            return self.gt_list[val+1]
        
        @check_small_positive_decorator
        def between(self, min_val:int, max_val:int) -> int:
            if min_val > max_val:
                raise ValueError('Wrong interval. First parameter must be less than the second one.')

            n_a = self.less(max_val + 1)
            n_b = self.greater(min_val - 1)

            # n(A∩B) = n(A) + n(B) − n(A∪B)
            return n_a + n_b - len(self.list) # in Python len() function have O(1) complexity
    
    # Methods
    @check_small_positive_decorator
    def add(self, val: int) -> None:        
        self.list.append(val)
    
    def build_stats(self) -> DataStat:
        return self.DataStat(self.list)
