import unittest
from data_statistics.data_classes import DataCapture

class TestData(unittest.TestCase):
    def test_correct_add(self):
        ''' test a correct adding in DataCapture'''
        obj = DataCapture()
        result = obj.add(45)
        self.assertIsNone(result)

    def test_incorrect_negative_add(self):
        ''' test a wrong adding in DataCapture. No values less than 0 must be acepted'''
        val = -2
        obj = DataCapture()
        with self.assertRaises(ValueError):
            obj.add(val)
    
    def test_incorrect_str_add(self):
        ''' test a wrong adding in DataCapture. Only integer type values  must be acepted.'''
        val = '20'
        obj = DataCapture()
        with self.assertRaises(TypeError):
            obj.add(val)
    
    def test_incorrect_greater_add(self):
        ''' test a wrong adding in DataCapture. No values great than 1000 must be acepted'''
        val = 1000
        obj = DataCapture()
        with self.assertRaises(Exception):
            obj.add(val)

    def __load_set(self):
        '''Preparind data for test statistics methods'''
        #data_set = [3, 9, 3, 4, 6]
        data_set = [4, 3, 6, 4, 8, 8, 7, 6, 6, 5, 5, 3, 5, 2, 2, 2, 3, 4]
        obj = DataCapture()
        for val in data_set:
            obj.add(val)
        
        return obj.build_stats()

    
    def test_less(self):
        ''' test that less() method return all numbers lees than a given value.'''
        obj = self.__load_set()
        result = obj.less(4)
        #self.assertEqual(result, 2)
        self.assertEqual(result, 6)
    
    def test_incorrect_less_value(self):
        ''' test that less() method only acept as parameter integer values.'''
        obj = self.__load_set()
        with self.assertRaises(TypeError):
            obj.less('67')

    def test_incorrect_betweens(self):
        '''test tnat in the between(X, Y) method, X must be less than or equal to Y'''
        obj = self.__load_set()
        with self.assertRaises(ValueError):
            obj.between(6, 3)
    
    def test_incorrect_betweens_value(self):
        '''test that between(X, Y) method only acept as parameter integer values.'''
        obj = self.__load_set()
        with self.assertRaises(TypeError):
            obj.between(6, '3')
        

    def test_correct_betweens(self):
        ''' test that between(X, Y) method return all numbers greater than or equal to X and less than or equel to Y.'''
        obj = self.__load_set()
        result = obj.between(3, 6)
        #self.assertEqual(result, 4)
        self.assertEqual(result, 12)

    def test_greater(self):
        ''' test that greater() method return all numbers greater than a given value.'''
        obj = self.__load_set()
        result = obj.greater(4)
        #self.assertEqual(result, 2)
        self.assertEqual(result, 9)
    
    def test_incorrect_greater_value(self):
        ''' test that less() method only acept as parameter integer values.'''
        obj = self.__load_set()
        with self.assertRaises(TypeError):
            obj.greater('67')


if __name__ == '__main__':
    unittest.main()