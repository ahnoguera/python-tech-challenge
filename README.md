# Basic statistics

This project provides you with the needed classes to compute some basic statistics over a collection of small positive integers. What statistics can you compute?
- Get the amount of numbers in the collection less than a given input.
- Get the amount of numbers in the collection greater than a given input.
- Get the amount of numbers in the collection between a given interval.

All these statistics will run with complexity O(1) no matter the collection's size.

**NOTE**: It's important to mention that we assume a zero (0) number as an integer.

## Installation
For this project, you only need to be installed [python](https://www.python.org/downloads/).
To use the project's classes just clone the repo or download the [```data_classes.py```](data_statistics/data_classes.py) file.


## How to Use the Project
### DataCapture class
This class is for collecting data. 

To collect data, the class exposes the ```DataCapture.add()``` method. This method only acept small positive integers. The signature for the method is ```DataCapture.add(val: int) -> None```. ```TypeError``` exception can be obtained if the value to add is not an integer. Also, you can get a ```ValueError``` exception if the value to add is greater than 1000 or less than 0.

The following example shows how can be added numbers to the collection:

```python
from data_classes import DataCapture

capture = DataCapture()
capture.add(3)
capture.add(9)
capture.add(3)
capture.add(4)
capture.add(6)
```
and that's it. You have added numbers to your collection, but how can you get the statistics?

```DataCapture``` also provide the method ```DataCapture.build_stats()```, which return a ```DataStat``` class object.

### DataStat class
This class is an inner class of ```DataCapture```. So, to obtain an object with the statistics of the collection added before you must call the method ```build_stats()``` of  ```DataCapture``` class object.

To query the statistics of the collection, this class exposes three main methods:
- ```DataStat.less(val: int)```. Return the amount of numbers in the collection less than *val* parameter. Raise a ```TypeError``` if **val** is not an integer.
- ```DataStat.greater(val: int)```. Return the amount of numbers in the collection greater than *val* parameter. Raise a ```TypeError``` if **val** is not an integer.
- ```DataStat.between(min_val: int, max_val: int)```. Return the amount of numbers in the collection between both **min_val** and **max_val** parameters. Raise a ```TypeError``` if **min_val** or **max_val** are not integers, also can raise a ```ValueError``` exception if **min_val** is greater than **max_val**.

The following example shows how to obtain the stat object and use it:

```python
stat = capture.build_stats()
stat.less(4) # should return 2 (only two values 3, 3 are less than 4)
stat.between(3, 6) # should return 4 (3, 3, 4 and 6 are between 3 and 6)
stat.greater(4) # should return 2 (6 and 9 are the only two values greater than 4)
```
It's easy! Isn't it?